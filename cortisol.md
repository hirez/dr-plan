# Cortisol bingo.

* Food cravings
* Flushed face
* Muscle weakness
* Increased thirst
* Urinating more frequently
* Rapid weight gain in the face and abdomen
* Severe fatigue
* Irritability
* Brain fog
* Anxiety
* Disturbed sleep
* 'Tired but wired'
* Depression


The exploit here is that the brain fog and anx will make you think there are several wrong things flying in close
formation, and the food cravings, poor sleep and fatigue will convince you that you can barely move from the sofa
without risking collapse and/or what the roadies call 'bonking'. Oh, and the weight gain and depression will do absolute
wonders for your sense of self-worth.

## Potential fixes.

Yoga. Yes, really. Experiments seem to show that fifteen minutes of 'restorative' yoga of a morning will sort a body out
quick-smart. Chair yoga if you've managed to fecker yr knee, and a sturdy half-hour of Hatha on alternate days if not.

(All these terms lifted from the 'Down Dog' yoga app, which is very good indeed.)

Once you're less prone to food cravings and adrenaline spikes/crashes, getting outdoors for exercise is a Good Thing.
