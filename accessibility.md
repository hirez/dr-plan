# Caveats and apologies

Since I'm physically able, I've made a set of assumptions about what can and can't be achieved regarding movement and
access. If that's not you, and you feel up to it, I would be really very pleased to learn about alternative strategies.
