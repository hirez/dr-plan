# README #

This README would normally document whatever steps are necessary to get your brain up and running.

### What is this repository for? ###

* Quick summary: Because I am a hacker, I needed a personal DR plan. So.
* Version: 0.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Deployment instructions:

* Begin with [core.md.](core.md)

* Optionally consider [gc.md](gc.md) (that's garbage collection, not gender-critical. Though the latter is also
garbage..)

* [cortisol.md](cortisol.md) may help with debugging.

* [accessibility.md](accessibility.md) is me making excuses.

* For full ISO/ITIL compatability, print this out and have it handy. Then when you are _in extremis_ you have a checklist
in front of you. I mean, you already know the purpose and function of a disaster-recovery plan for computer systems.
It's just one of those, but closer to home.

### Contribution guidelines ###

* Patches welcome, but be cautious of evangelism in all its forms.

### Who do I talk to? ###

* Me: julia@mysparedomain.com / twitter: @\_JHR\_ 

### Changelog

* 0.1: 2020-05-25 / reformat core.md, create gc.md
* 0.2: 2020-05-25 / create cortisol.md and accessibility.md
* 0.3: 2020-11-01 / minor formatting fix.
