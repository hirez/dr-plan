# Garbage collection

## Theory of operation

We all have different mental models of how brains work or don't work. I've joked before that my personal demons are
actually daemons, and if you're familiar with that reference, you're in the right place.

When a unix box has too many things to do, it starts to slow down as more and more vital things that should be in core
memory are pushed out to disk. Soon the box is doing nothing useful but paging things back and forth. Where I'm from, we
call this 'thrashing'.

Often the only way out is a theraputic reboot.

We don't theraputically reboot people. I mean, we _did,_ but ECT has been considered barbaric for many years.

## How do I know when I'm thrashing?

If you can't concentrate worth a light because there are too many things to think about, and/or your internal daemons
are loud enough that you really can't hear yrself think, then you may well be thrashing.

## How do I make it stop?

It's time find all the pointers to memory-hogs or files held open my long-running processes and tidy them up a bit.
Whatever dreadful analogy works best for you.

* Construct a list. On a post-it or the back of an envelope. The important work is externalising it by writing it down. This is not a 'to-do' list, because that implies actions you may not be ready to take, which implies
  some judgement and oh god i can't make it stop make it stop.

* There's a chance you already feel better about some things. Yay! Progress!

* If you feel able, tackle a thing on the list. Maybe you can almost feel the noise in yr head drop as the daemon
  process is killed and the memory and/or disk is freed.

* Loop for as long as you like.

## Bonus level.

If you _really_ want to rochambeau (Southpark version) some daemons, consider lifting concepts wholesale from Kanban:

* Write each thing on yr list out as a separate post-it.

* Tag them if you feel able: to-do, blocked, won't-do. 

* Feel free to ritually destroy the won't-do collection.

* Stop before it all becomes too organised and is an end in itself.
